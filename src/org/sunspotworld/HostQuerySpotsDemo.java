/*
 * Copyright (c) 2010 Oracle.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package org.sunspotworld;

import com.sun.spot.client.DummySpotClientUI;
import com.sun.spot.client.IUI;
import com.sun.spot.client.SpotClientCommands;
import com.sun.spot.client.SpotClientException;
import com.sun.spot.client.command.GetPowerStatsCmd;
import com.sun.spot.client.command.HelloResult;
import com.sun.spot.client.command.HelloResultList;
import com.sun.spot.io.j2me.radiostream.RadiostreamConnection;
import com.sun.spot.peripheral.NoRouteException;
import com.sun.spot.peripheral.Spot;
import com.sun.spot.peripheral.ota.ISpotAdminConstants;
import com.sun.spot.util.IEEEAddress;


import java.io.*;

import java.util.Properties;
import javax.microedition.io.Connector;

/**
 * Sample Sun SPOT client application.
 *
 * This application demonstrates how to locate nearby SPOTs and then send them
 * commands over-the-air (OTA).
 *
 * Please refer to the section "Using the SPOT Client" in the SPOT Developer's
 * Guide for details. Also refer to the hostjavadoc in the SDK/doc folder.
 *
 */
public class HostQuerySpotsDemo
{

    // parameters needed by SpotClientCommands constructor
    private IUI ui = new DummySpotClientUI(false);
    private String pathToApp = ".";
    private String commPort = System.getProperty("SERIAL_PORT");    // set by host-run script
    private int echoPort = ISpotAdminConstants.MASTER_ISOLATE_ECHO_PORT;
    private File sunspotArmDir;
    private String libPath;
    private String keystorePath;

    public HostQuerySpotsDemo() throws IOException
    {
        Properties p = new Properties();
        String userHome = System.getProperty("user.home");
        p.load(new FileInputStream(new File(userHome, ".sunspot.properties")));
        String sunspotHome = p.getProperty("sunspot.home");
        sunspotArmDir = new File(sunspotHome, "arm");
        libPath = sunspotHome + File.separator + "arm"
                + File.separator + p.getProperty("spot.library.name");
        keystorePath = userHome + File.separator + "sunspotkeystore";
    }

    /**
     * Discover any nearby SPOTs and send them some OTA commands.
     *
     * @throws IOException
     */
    public void discover() throws IOException
    {

        SpotClientCommands broadcastCommandRepository = new SpotClientCommands(
                ui,
                pathToApp,
                libPath,
                sunspotArmDir,
                keystorePath,
                commPort,
                null,
                echoPort);

        System.out.println("Sending Hello command...");
        HelloResultList helloList = (HelloResultList) broadcastCommandRepository.execute("hello", "5000", "8", "broadcast", null);
        System.out.println("Hello details: " + helloList.toString());

        // now send some OTA commands to all the SPOTs we just found
        int cnt = 1;
        for (Object w : helloList)
        {
            HelloResult who = (HelloResult) w;
            if (who.isSpot())
            {
                String remoteAddress = IEEEAddress.toDottedHex(who.remoteAddress);
                System.out.println("\n\n" + (cnt++) + ": " + remoteAddress);
                sendOTAcommands(remoteAddress, 5);
            }
        }
    }

    /**
     * Send some OTA commands to the specified SPOT. In particular blink the
     * SPOT's LEDs & then get its power statistics.
     *
     * @param remoteAddress IEEE address of SPOT to send OTA commands
     */
    public void sendOTAcommands(String remoteAddress, Integer num)
    {

        SpotClientCommands commandRepository = null;
        try
        {
            commandRepository = new SpotClientCommands(
                    ui,
                    pathToApp,
                    libPath,
                    sunspotArmDir,
                    keystorePath,
                    commPort,
                    remoteAddress,
                    echoPort);
        }
        catch (IOException ex)
        {
            System.out.println("Error opening OTA connection to " + remoteAddress);
            return;
        }

        try
        {
            commandRepository.execute("synchronize");   // all OTA sessions start with a sync
            commandRepository.execute("blink", num);      // blink the SPOT's LEDs
            GetPowerStatsCmd.Result powerStats = // get power statistics from SPOT
                    (GetPowerStatsCmd.Result) commandRepository.execute("getpowerstats");
            //System.out.println(powerStats.toString());
            //System.out.println("\nBattery level = " + powerStats.getBatteryLevel() + "%");
        }
        catch (IOException iex)
        {
            System.out.println("Error executing remote command: " + iex);
        }
        catch (SpotClientException ex)
        {
            System.out.println("Error executing remote command: " + ex);
        }
        finally
        {
            try
            {
                commandRepository.execute("quit");      // all OTA sessions end with a quit
            }
            catch (Exception ex)
            {
                System.out.println("Error ending session: " + ex);
            }
        }
    }

    public static String readTxtFile(String filePath)
    {
        try
        {
            String encoding = "UTF-8";
            File file = new File(filePath);
            if (file.isFile() && file.exists())
            { //????????
                InputStreamReader read = new InputStreamReader(
                        new FileInputStream(file), encoding);//???????
                BufferedReader bufferedReader = new BufferedReader(read);
                String lineTxt = null;
                while ((lineTxt = bufferedReader.readLine()) != null)
                {
                    //System.out.println(lineTxt);
                    return lineTxt;

                }
                read.close();
            }
            else
            {
                System.out.println("cound'nt read");
                return "";
            }
        }
        catch (Exception e)
        {
            System.out.println("error");
            //e.printStackTrace();
            return "";
        }
        return "";
    }

    public static void removeNthLine(String f, int toRemove) throws IOException
    {

        File tmp = File.createTempFile("tmp", "");

        BufferedReader br = new BufferedReader(new FileReader(f));
        BufferedWriter bw = new BufferedWriter(new FileWriter(tmp));

        for (int i = 0; i < toRemove; i++)
        {
            bw.write(String.format("%s%n", br.readLine()));
        }

        br.readLine();

        String l;
        while (null != (l = br.readLine()))
        {
            bw.write(String.format("%s%n", l));
        }

        br.close();
        bw.close();

        File oldFile = new File(f);
        if (oldFile.delete())
        {
            tmp.renameTo(oldFile);
        }

    }

    public static void sendCommand(String spot, String command)
    {
        try
        {
            String dest = "radiostream://" + spot + ":100";
            System.out.println(dest);
            RadiostreamConnection conn =
                    (RadiostreamConnection) Connector.open(dest);
            DataOutputStream dos = conn.openDataOutputStream();
            DataInputStream dis = conn.openDataInputStream();
            System.out.println("Opened connection to spot");
            try
            {
                dos.writeUTF(command);
                System.out.println("Sent command");
                dos.flush();
                System.out.println(spot + " responds with " + dis.readUTF());
            }
            catch (NoRouteException e)
            {
                System.out.println("No route to " + spot);
            }
            finally
            {
                dis.close();
                dos.close();
                conn.close();
            }
        }
        catch (IOException e)
        {
            System.out.println("Can't communicate with SPOT " + spot + "\n" + e.getMessage());
        }

    }

    /**
     * Start up the host application.
     */
    public static void main(String[] args) throws Exception
    {
        HostQuerySpotsDemo app = new HostQuerySpotsDemo();
        //app.discover();
//        app.sendOTAcommands("0014.4F01.0000.7D2E");



//        System.exit(0); 
//        System.out.println("Hello");
//        ServerSocket server=new ServerSocket(5678);   
//        Socket client=server.accept();   
//        BufferedReader in=  
//        new BufferedReader(new InputStreamReader(client.getInputStream()));   
//        PrintWriter out=new PrintWriter(client.getOutputStream());   
//        while(true){   
//        String str=in.readLine();   
//        System.out.println(str);  
//        
//        if(str.equals("blink"))
//        {
//        app.sendOTAcommands("0014.4F01.0000.7D2E");
//        //System.exit(0); 
//        }
//        out.println("has receive....");   
//        out.flush();   
//            if(str.equals("end")) 
//            {
//            break; 
//            
//            }
//        }   
//        client.close();   

//        String filePath = "/Users/huangkaituo/Desktop/_HostQuerySpotsDemo/commandQueue.spot";
        String filePath = "/Users/Raphy/Documents/School/Fall2013/EC544/challenge-3/commandQueue.spot";


        while (true)
        {
            String str = readTxtFile(filePath);

            //read a number from text file, do the OTA commands
            if (str != "")
            {
                //Parse line to ID and int
                String delims = "[ ]+";
                String[] tokens = str.split(delims);

//                if (tokens[1].equals("ON") || tokens[1].equals("OFF"))
//                {
//                    sendCommand(tokens[0], tokens[1]);
//                }
//                else
//                {
                    int num = Integer.parseInt(tokens[1]);
                    System.out.println(str);

                    //Thread.sleep(200);
                    app.sendOTAcommands(tokens[0], num);
//                }

                removeNthLine(filePath, 0);
            }
        }

    }
}
