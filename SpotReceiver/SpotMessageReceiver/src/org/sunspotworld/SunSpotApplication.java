/*
 * SunSpotApplication.java
 *
 * Created on Oct 3, 2013 9:30:23 AM;
 */
package org.sunspotworld;

import com.sun.spot.io.j2me.radiostream.RadiostreamConnection;
import com.sun.spot.peripheral.NoRouteException;
import com.sun.spot.peripheral.Spot;
import com.sun.spot.peripheral.radio.RadioFactory;
import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.ITriColorLEDArray;
import com.sun.spot.service.BootloaderListenerService;
import com.sun.spot.util.IEEEAddress;

import java.io.DataInputStream;

import java.io.DataOutputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

/**
 * The startApp method of this class is called by the VM to start the
 * application.
 * 
 * This application waits for messages and then performs the desired task.
 */
public class SunSpotApplication extends MIDlet {

    private ITriColorLEDArray leds = (ITriColorLEDArray) Resources.lookup(ITriColorLEDArray.class);

    protected void startApp() throws MIDletStateChangeException {
        System.out.println("Hello, world");
        BootloaderListenerService.getInstance().start();   // monitor the USB (if connected) and recognize commands from host

        long ourAddr = RadioFactory.getRadioPolicyManager().getIEEEAddress();
        System.out.println("Our radio address = " + IEEEAddress.toDottedHex(ourAddr));

//        ISwitch sw1 = (ISwitch) Resources.lookup(ISwitch.class, "SW1");
        ITriColorLED base = leds.getLED(0);
        base.setRGB(100,0,0);                    // set color to moderate red
        base.setOn(); /* Just so we know that it is working */
//        while (sw1.isOpen()) {                  // done when switch is pressed
//            led.setOn();                        // Blink LED
//            Utils.sleep(250);                   // wait 1/4 seconds
//            led.setOff();
//            Utils.sleep(1000);                  // wait 1 second
//        }
//        notifyDestroyed();                      // cause the MIDlet to exit

        /* Infinite loop of listening */
        while (true) 
        {
            try 
            {
                RadiostreamConnection conn =
                        (RadiostreamConnection) Connector.open("radiostream://0014.4F01.0000.7E8B:100");
                Spot.getInstance().getRadioPolicyManager().setChannelNumber(26);
                Spot.getInstance().getRadioPolicyManager().setPanId((short)45);
                
                DataInputStream dis = conn.openDataInputStream();
                DataOutputStream dos = conn.openDataOutputStream();
                System.out.println("Connection open");
                try {
                    String question = dis.readUTF();
                    System.out.println("Received message");
                    if (question.equals("ON")) 
                    {
                        dos.writeUTF("Roger");
                        for (int ii = 0 ; ii < 8 ; ii++)
                        {
                            ITriColorLED led = leds.getLED(ii);
                            led.setRGB((ii + 1) * 30, (ii + 1) * 30, (ii + 1) * 30);
                            led.setOn();
                            System.out.println("Turning " + ii + " on.");
                        }
                    } 
                    else if (question.equals("OFF"))
                    {
                        dos.writeUTF("Roger");
                        for (int ii = 0 ; ii < 8 ; ii++)
                        {
                            ITriColorLED led = leds.getLED(ii);
                            led.setOff();
                        }
                    }
                    else
                    {
                        dos.writeUTF("ERROR");
                    }
                    dos.flush();
                } 
                catch (NoRouteException e) 
                {
                    System.out.println("No route to 0014.4F01.0000.7E8B");
                } 
                finally 
                {
                    dis.close();
                    dos.close();
                    conn.close();
                }
            }
            catch (IOException e)
            {
                System.out.println("IOException on spot");
            }
        }
    }

    protected void pauseApp() {
        // This is not currently called by the Squawk VM
    }

    /**
     * Called if the MIDlet is terminated by the system.
     * It is not called if MIDlet.notifyDestroyed() was called.
     *
     * @param unconditional If true the MIDlet must cleanup and release all resources.
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
    }
}
